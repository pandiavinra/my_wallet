<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1/init', '')->group(function() {
    Route::post('/', 'InitWalletController@init');
});

Route::prefix('/v1/wallet', '')->group(function() {
    Route::post('/', ['uses'=>'WalletController@enable', 'middleware' => ['auth.api']]);
    Route::get('/', ['uses'=>'WalletController@getWallet', 'middleware' => ['auth.api']]);
    Route::patch('/', ['uses'=>'WalletController@disabled', 'middleware' => ['auth.api']]);

    Route::post('/deposits', ['uses'=>'DepositsController@deposits', 'middleware' => ['auth.api']]);
    Route::post('/withdrawals', ['uses'=>'WithdrawalsController@withdrawals', 'middleware' => ['auth.api']]);
});

