<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;

class Wallet extends BaseModel
{
    protected $table = 'wallets';
    protected $fillable = ['owned_by', 'balance', 'status', 'token'];

//    public static function boot()
//    {
//        parent::boot();
//        self::creating(function ($model) {
//            $model->owned_by = (string) Uuid::generate(4);
//        });
//    }
}