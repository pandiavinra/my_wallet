<?php

namespace App\Models;

class Transaction extends BaseModel
{
    protected $table = 'transactions';
    protected $fillable = ['reference_id', 'status', 'type', 'amount', 'wallet_id'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->status = "success";
        });
        self::created(function ($model) {
            $wallet = $model->wallet;
            if ($model->type=="deposits"){
                $wallet->balance = $wallet->balance + $model->amount;
            }else{
                $wallet->balance = $wallet->balance - $model->amount;
            }
            $wallet->save();
        });
    }

    public function wallet(){
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

}