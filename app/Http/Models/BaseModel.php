<?php
namespace App\Models;

use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class BaseModel extends Model
{

    use HasUUID;
    public static function boot()
    {
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

}
