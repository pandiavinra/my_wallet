<?php
namespace App\Http\Middleware;

use App\Models\Wallet;
use Closure;
use Illuminate\Support\Str;

class Auth
{

    public function handle($request, Closure $next)
    {
        $token =  $request->header('Authorization', '');
        if (Str::startsWith($token, 'Token ')) {
            $token = Str::substr($token, 6);
            $explodeToken = explode(" ", $token);
            $token = $explodeToken[0];
        }else{
            $token ="";
        }

        if(empty($token)){
            $data = array(
                'status' => "error",
                'message' => "token not valid or not provided"
            );
            return \Response::json($data, 403);

        }else {
            $wallet = Wallet::where("token", $token)->first();
            if (!$wallet) {
                $data = array(
                    'status' => "error",
                    'message' => "token not registered"
                );
                return \Response::json($data, 403);
            }
        }
        return $next($request);
    }
}