<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class WalletController extends Controller
{

    public function enable(Request $request){
        try{
            $token = $this->getToken($request);
            $wallet = Wallet::where("token", $token)->first();
            $wallet->status=1;
            $wallet->save();
            $data = array(
                "wallet" => $this->parsingWalletResponse($wallet)
            );
            return \Response::json(array("status" => "success", "data" => $data), 201);

        }catch (\Exception $e){
            return $this->returnErrorResponse($e);
        }
    }

    public function disabled(Request $request){
        try{
            $token = $this->getToken($request);
            $wallet = Wallet::where("token", $token)->first();
            $wallet->status=0;
            $wallet->save();
            $data = array(
                "wallet" => $this->parsingWalletResponse($wallet)
            );
            return \Response::json(array("status" => "success", "data" => $data), 201);

        }catch (\Exception $e){
            return $this->returnErrorResponse($e);
        }
    }

    public function getWallet(Request $request){
        try{
            $token = $this->getToken($request);
            $wallet = Wallet::where("token", $token)->first();
            if ($wallet->status==1){
                $data = array(
                    "wallet" => $this->parsingWalletResponse($wallet)
                );
            }else{
                $response = array(
                    'status' => "error",
                    'message' => "Wallet disabled"
                );
                return \Response::json($response, 400);
            }
            return \Response::json(array("status" => "success", "data" => $data), 200);

        }catch (\Exception $e){
            return $this->returnErrorResponse($e);
        }
    }

    protected function parsingWalletResponse($wallet){
        return array(
            "id"        => $wallet->uuid,
            "owned_by"  => $wallet->owned_by,
            "status"    => ($wallet->status==1) ? "enabled" : "disabled",
            "enabled_at"=> $wallet->updated_at->toDateTimeString(),
            "balance"   => $wallet->balance,
        );
    }

}
