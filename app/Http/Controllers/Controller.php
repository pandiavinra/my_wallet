<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $token=null;

    protected function returnErrorResponse($e){
        $response = array(
            'status' => "error",
            'message' => $e->getMessage().$e->getFile().$e->getLine()
        );
        return \Response::json($response, 500);
    }

    protected function getToken($request){
        $token =  $request->header('Authorization', '');
        if (Str::startsWith($token, 'Token ')) {
            $token = Str::substr($token, 6);
            $explodeToken = explode(" ", $token);
            $token = $explodeToken[0];
        }else{
            $token ="";
        }
        return $token;
    }
}
