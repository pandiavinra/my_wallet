<?php
namespace App\Http\Controllers;


use App\Models\Wallet;
use Illuminate\Http\Request;
class InitWalletController extends Controller
{
    public function init(Request $request){
        try{
            $params = $request->all();
            if (isset($params['customer_xid'])){
                $wallet = Wallet::where("owned_by", $params['customer_xid'])->first();
                if (!$wallet){
                    $token = strtolower(str_random(40));
                    $wallet = Wallet::create(
                        array(
                            "token" => $token,
                            "owned_by" => $params['customer_xid'],
                            "status" => 1,
                            "balance" => 0,
                        )
                    );
                }
                $data = array(
                    "token" => $wallet->token
                );
                return \Response::json(array("status" => "success", "data" => $data), 201);
            }else{
                $response = array(
                    'status' => "error",
                    'message' => "customer_xid not provided"
                );
                return \Response::json($response, 400);
            }

        }catch (\Exception $e){
            return $this->returnErrorResponse($e);
        }
    }
}

