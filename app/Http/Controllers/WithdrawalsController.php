<?php
namespace App\Http\Controllers;


use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\Request;
class WithdrawalsController extends Controller
{
    public function withdrawals(Request $request){
        try{
            $params = $request->all(0);
            $token = $this->getToken($request);
            $wallet = Wallet::where("token", $token)->first();
            if ($wallet->status==1){
                if ($wallet->balance>=$params['amount']){
                    $transaction = Transaction::where('reference_id', $params['reference_id'])->first();
                    if (!$transaction){
                        $params['type'] = 'withdraw';
                        $params['wallet_id'] = $wallet->id;
                        $withdrawal = Transaction::create($params);
                        $data = array(
                            "deposit" => $this->parsingWithdrawalResponse($withdrawal)
                        );
                        return \Response::json(array("status" => "success", "data" => $data), 400);
                    }else{
                        $response = array(
                            'status' => "error",
                            'message' => "reference_id already exist"
                        );
                        return \Response::json($response, 400);
                    }
                }else{
                    $response = array(
                        'status' => "error",
                        'message' => "balance must more than withdrawal amount"
                    );
                    return \Response::json($response, 400);
                }
            }else{
                $response = array(
                    'status' => "error",
                    'message' => "Wallet disabled"
                );
                return \Response::json($response, 400);
            }

        }catch (\Exception $e){
            return $this->returnErrorResponse($e);
        }
    }

    protected function parsingWithdrawalResponse($withdrawal){
        return array(
            "id"            => $withdrawal->uuid,
            "withdrawn_by"    => $withdrawal->wallet->owned_by,
            "status"        => $withdrawal->status,
            "withdrawn_at"    => $withdrawal->updated_at->toDateTimeString(),
            "amount"        => $withdrawal->amount,
            "reference_id"   => $withdrawal->reference_id,
        );
    }
}
