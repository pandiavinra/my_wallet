<?php
namespace App\Http\Controllers;


use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\Request;
class DepositsController extends Controller
{
    public function deposits(Request $request){
        try{
            $params = $request->all(0);
            $token = $this->getToken($request);
            $wallet = Wallet::where("token", $token)->first();
            if ($wallet->status==1){
                $transaction = Transaction::where('reference_id', $params['reference_id'])->first();
                if (!$transaction){
                    $params['type'] = 'deposits';
                    $params['wallet_id'] = $wallet->id;
                    $deposit = Transaction::create($params);
                    $data = array(
                        "deposit" => $this->parsingDepositResponse($deposit)
                    );
                    return \Response::json(array("status" => "success", "data" => $data), 200);
                }else{
                    $response = array(
                        'status' => "error",
                        'message' => "reference_id already exist"
                    );
                    return \Response::json($response, 400);
                }
            }else{
                $response = array(
                    'status' => "error",
                    'message' => "Wallet disabled"
                );
                return \Response::json($response, 400);
            }

        }catch (\Exception $e){
            return $this->returnErrorResponse($e);
        }
    }

    protected function parsingDepositResponse($deposit){
        return array(
            "id"            => $deposit->uuid,
            "deposit_by"    => $deposit->wallet->owned_by,
            "status"        => $deposit->status,
            "deposit_at"    => $deposit->updated_at->toDateTimeString(),
            "amount"        => $deposit->amount,
            "reference_id"   => $deposit->reference_id,
        );
    }

}


